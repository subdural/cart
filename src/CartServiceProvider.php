<?php namespace Robb\Cart;

use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('Cart', function($app)
        {
            $storage = $app->make('Illuminate\Session\Store');
            return new Cart($storage);
        });
    }

}
