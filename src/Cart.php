<?php namespace Robb\Cart;

use Illuminate\Session\Store

class Cart
{
    public function __construct(Store $storage)
    {
        $this->storage = $storage;
    }

    public function add($id, $name, $quantity, $price)
    {
        $cart = $this->all();

        if (empty($cart)) $cart = [];

        if (array_key_exists($id, $cart))
        {
            $qty = $cart[$id]['qty'];
            $quantity = $cart[$id]['qty'] = ++$qty;
        }

        $this->storage->put('cart.' . $id, ['name' => $name, 'qty' => $quantity, 'price' => $price]);
    }

    public function all()
    {
        return $this->storage->get('cart');
    }

    public function quantity()
    {
        # TODO: post here to set quatity based on product id
    }

    function totalCost()
    {
        $total = 0;

        foreach ($this->storage->get('cart') as $item)
        {
            $total += $item['price'] * $item['qty'];
        }

        return $total;
    }

    public function itemCount()
    {
        $total = 0;

        foreach ($this->items as $item)
        {
            $total += $item['quantity'];
        }

        return $total;
    }

    public function remove($id)
    {
        $this->storage->forget('cart.' . $id);
    }

    public function clear()
    {
        $this->storage->forget('cart');
    }
}
